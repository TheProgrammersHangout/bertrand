/*
 * This file is generated by jOOQ.
 */
package uk.co.markg.bertrand.db;


import uk.co.markg.bertrand.db.tables.Channels;
import uk.co.markg.bertrand.db.tables.Messages;
import uk.co.markg.bertrand.db.tables.Users;


/**
 * Convenience access to all tables in 
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>channels</code>.
     */
    public static final Channels CHANNELS = Channels.CHANNELS;

    /**
     * The table <code>messages</code>.
     */
    public static final Messages MESSAGES = Messages.MESSAGES;

    /**
     * The table <code>users</code>.
     */
    public static final Users USERS = Users.USERS;
}
