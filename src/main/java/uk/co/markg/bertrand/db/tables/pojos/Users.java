/*
 * This file is generated by jOOQ.
 */
package uk.co.markg.bertrand.db.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Users implements Serializable {

    private static final long serialVersionUID = -2002956536;

    private Long userid;

    public Users() {}

    public Users(Users value) {
        this.userid = value.userid;
    }

    public Users(
        Long userid
    ) {
        this.userid = userid;
    }

    public Long getUserid() {
        return this.userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Users (");

        sb.append(userid);

        sb.append(")");
        return sb.toString();
    }
}
